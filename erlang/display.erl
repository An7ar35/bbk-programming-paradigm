%% @author Es A. Davison

-module(display).

%% ====================================================================
%% API functions
%% ====================================================================
-export([loop/0]).

%% ====================================================================
%% Internal functions
%% ====================================================================
loop() ->
	receive
		{Farhenheit,Celsius} ->
			io:format("~p Farhenheit = ~p Celsius~n", [Farhenheit,Celsius]),
			loop()
	end.