%% @author Es A. Davison

-module(controller).

%% ====================================================================
%% API functions
%% ====================================================================
-export([convertTest/0]).
-export([convertTemp/2]).



%% ====================================================================
%% Internal functions
%% ====================================================================
convertTest() ->
	Convert_PID = spawn(fun convert:convert/0),
	Convert_PID ! {"ConvertToFarhenheit",100},
	Convert_PID ! {"ConvertToCelsius", 200},
	Convert_PID ! {"ConvertToFarhenheit", 90},
	Convert_PID ! {"ConvertToCelsius", 194}.

convertTemp(Conversion,Temperature) ->
	Convert_PID = spawn(fun convert:convert/0),
	Convert_PID ! {Conversion, Temperature}.