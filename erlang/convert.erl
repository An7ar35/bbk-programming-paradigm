%% @author Es A. Davison

-module(convert).

%% ====================================================================
%% API functions
%% ====================================================================
-export([convert/0]).



%% ====================================================================
%% Internal functions
%% ====================================================================
convert() ->
	Display_PID = spawn(fun display:loop/0),
	receive
		{Conversion,T} ->
			case Conversion of
				"ConvertToCelsius" ->
					Temperature = (T-32)*(5/9),
					Display_PID ! {Temperature,T},
					convert();
				"ConvertToFarhenheit" ->
					Temperature = T*(9/5)+32,
					Display_PID ! {T,Temperature},				
					convert();
				_ ->
					convert()
			end;
		_ ->
			convert()
	end.