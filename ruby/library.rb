require 'singleton'
require 'csv'
require 'english'

##################
# Calendar Class #
##################
class Calendar
  include Singleton
  #--------------------------------#
  # Initialises the Calendar class #
  #--------------------------------#
  def initialize()
    @date = 0
  end

  #---------------#
  # Gets the date #
  #---------------#
  def get_date()
    return @date
  end

  #-----------------------#
  # Advance the date by 1 #
  #-----------------------#
  def advance()
    @date += 1
    return @date
  end

  #--------------------------#
  # Sets the calendar's date #
  #--------------------------#
  def set_date( date )
    @date = date
  end
end

##############
# Book Class #
##############
class Book
  #-----------------------------#
  #  Initialises the Book class #
  #-----------------------------#
  def initialize( id, title, author )
    @id = id
    @title = title
    @author = author
    @due_date = nil
  end

  #--------------------#
  # Gets the book's ID #
  #--------------------#
  def get_id()
    return @id
  end

  #-----------------------#
  # Gets the book's title #
  #-----------------------#
  def get_title()
    return @title
  end

  #------------------------#
  # Gets the book's author #
  #------------------------#
  def get_author()
    return @author
  end

  #--------------------------#
  # Gets the book's due date #
  #--------------------------#
  def get_due_date()
    return @due_date
  end

  #----------------------#
  #  Checks out the book #
  #----------------------#
  def check_out( due_date )
    @due_date = due_date
  end

  #--------------------#
  # Checks in the book #
  #--------------------#
  def check_in()
    @due_date = nil
  end

  #------------------------------#
  # Book information to a string #
  #------------------------------#
  def to_s()
    return @id.to_s + ": " + @title + ", by " + @author
  end
end

################
# Member Class #
################
class Member
  #------------------------------#
  # Initialises the Member class #
  #------------------------------#
  def initialize( name, library )
    @name = name
    @library = library
    @books_checked_out ||= []
  end

  #------------------------#
  # Gets the member's name #
  #------------------------#
  def get_name()
    return @name
  end

  #----------------------------------#
  # Checks out book from the library #
  #----------------------------------#
  def check_out( book )
    calendar = Calendar.instance
    book.check_out( calendar.get_date() + 7 )
    @books_checked_out << book
  end

  #-----------------------------#
  # Returns book to the library #
  #-----------------------------#
  def give_back( book )
    i = @books_checked_out.find_index( book )
    @books_checked_out[ i ].check_in()
    @books_checked_out.delete( book )
  end

  #-----------------------------#
  # Gets all the member's books #
  #-----------------------------#
  def get_books()
    return @books_checked_out
  end

  #---------------------------------------#
  # Sends an overdue notice to the member #
  #---------------------------------------#
  def send_overdue_notice( notice )
    puts get_name() + ": " + notice
  end

  #-------------------------------------------#
  # Loads a book back into the member's books #
  #-------------------------------------------#
  def load_loaned_book( book, due_date )
    book.check_out( due_date )
    @books_checked_out << book
  end
end

#################
# Library Class #
#################
class Library
  include Singleton
  #-------------------------------#
  # Initialises the Library class #
  #-------------------------------#
  def initialize()
    @libdata_filename = 'library.bin'
    @opened = false
    @members ||= []
    @collection ||= []
    @calendar = Calendar.instance
    #First time run
    if( File.file?( 'collection.txt' ) and !File.file?( @libdata_filename ) ) #first time the library is opened
      CSV.foreach( 'collection.txt', 'r:windows-1250' ) do |row|
        @collection << Book.new( $INPUT_LINE_NUMBER, row[0], row[1])
      end
    elsif( File.file?( @libdata_filename ) )
      load_everything()
    else
      raise "data file(s) not found."
    end
    @currentMember = nil
  end

  #---------------#
  # Opens library #
  #---------------#
  def open()
    if @opened
      raise "The library is already open!"
    else
      @opened = true
      @calendar.advance()
      return "Today is day " + @calendar.get_date().to_s
    end
  end

  #-------------------------#
  # Finds all overdue books #
  #-------------------------#
  def find_all_overdue_books()
    if @opened == false
      raise "The library is not open."
      return
    end
    overdue_books = ""
    overdue_flag = false
    @members.each { |m|
      books_of_member = m.get_books()
      books_of_member.each { |book|
        if book.get_due_date < @calendar.get_date()
          overdue_flag = true
          overdue_books += m.get_name() + ": " + book.to_s + "\n"
        end
      }
    }
    if overdue_flag == false
      overdue_books = "No books are overdue."
    end
    return overdue_books
  end

  #--------------------------#
  # Issues a membership card #
  #--------------------------#
  def issue_card( name_of_member )
    if @opened == false
      raise "The library is not open."
      return
    end
    if @members.any?{|m| (m.get_name).eql? name_of_member }
      return name_of_member + " already has a library card."
    else
      @members << Member.new(name_of_member,self)
    end
  end

  #-----------------#
  # Serves a member #
  #-----------------#
  def serve( name_of_member )
    if @opened == false
      raise "The library is not open."
      return
    end
    i = @members.find_index { |member| member.get_name == name_of_member }
    if i == nil
      return name_of_member + " does not have a library card."
    end
    @currentMember = @members[i]
    return "Now serving " + name_of_member + "."
  end

  #--------------------------------------#
  # Finds current member's overdue books #
  #--------------------------------------#
  def find_overdue_books()
    if @opened == false
      raise "The library is not open."
      return
    end
    if @currentMember == nil
      raise "No member being served at the moment."
      return
    else
      overdue_books = ""
      books_of_member = @currentMember.get_books()
      overdue_flag = false
      books_of_member.each { |book|
        if book.get_due_date < @calendar.get_date()
          overdue_flag = true
          overdue_books += book.to_s + "\n"
        end
      }
      if overdue_flag == false
        overdue_books = "None"
      end
      return overdue_books
    end
  end

  #-----------------#
  # Checks in books #
  #-----------------#
  def check_in(*book_numbers) # * = 1..n of book numbers
    if @opened == false
      raise "The library is not open."
      return
    end
    if @currentMember == nil
      raise "No member currently being served."
    else
      returned_queue ||= []
      books = @currentMember.get_books
      book_numbers.each { |id|
        i = books.find_index { |book_id| book_id.get_id() == id }
        if i == nil
          raise "The member does not have " + id.to_s
        else
          returned_queue << books[i]
        end
      }

      n_books_returned = returned_queue.count
      returned_queue.each { |book|
        @currentMember.give_back( book )
        @collection << book
      }
      return @currentMember.get_name() + " has returned " + n_books_returned.to_s + " book(s)."
    end
  end

  #----------------------------#
  # Searches for library books #
  #----------------------------#
  def search( string )
    if @opened == false
      raise "The library is not open."
      return
    end
    if string.length < 4
      return "Search string must contain at least four characters."
    end
    search_flag = false
    search_results = ""
    @collection.each { |book|
      if (book.get_title().downcase).include?( string.downcase ) || (book.get_author().downcase).include?( string.downcase )
        search_flag = true
        search_results += book.to_s + "\n"
      end
    }
    if search_flag == false
      return "No books found."
    end
    return search_results
  end

  #------------------#
  # Checks out books #
  #------------------#
  def check_out(*book_ids ) # 1..n book_ids
    if @opened == false
      raise "The library is not open."
      return
    end
    if @currentMember == nil
      raise "No member currently being served."
      return
    end
    if ( ( @currentMember.get_books() ).count + book_ids.count ) > 3
      raise "Member cannot checkout that many books."
      return
    else
      checkout_queue ||= []
      book_ids.each { |id|
        i = @collection.find_index { |book| book.get_id() == id }
        if i == nil
          raise "The library does not have " + id.to_s + "."
        else
          checkout_queue << @collection[i]
        end
      }

      n_books_checked_out = checkout_queue.count
      checkout_queue.each { |book|
        @currentMember.check_out( book )
        @collection.delete( book )
      }
      return n_books_checked_out.to_s + " book(s) have been checked out to " + @currentMember.get_name() + "."
    end
  end

  #---------------------------#
  # Renews due date for books #
  #---------------------------#
  def renew( *book_ids ) # 1..n book_ids
    if @opened == false
      raise "The library is not open."
      return
    end
    if @currentMember == nil
      raise "No member being served at the moment."
    else
      books = @currentMember.get_books
      book_ids.each { |id|
        i =  books.find_index { |book| book.get_id() == id }
        if i == nil
          raise "The member does not have book " + id.to_s + "."
        else
          books[i].check_in()
          books[i].check_out( @calendar.get_date + 7 )
        end
      }
    end
  end

  #--------------------#
  # Closes the library #
  #--------------------#
  def close()
    if @opened
      @currentMember = nil
      save_everything()
      @opened = false
      return "Good night."
    else
      raise "The library is not open."
    end
  end

  #--------------------------#
  # Quits the library system #
  #--------------------------#
  def quit()
    if @opened
      close()
    end
    return "The library is now closed for renovations."
  end

  #------------------------#
  # Saves all library data #
  #------------------------#
  def save_everything()
    data = Array.new
    data << @calendar.get_date() << @members << @collection
    File.open(@libdata_filename, "w") { |f| f.write( Marshal.dump(data) )  }
  end

  #-----------------------------#
  # Loads back all library data #
  #-----------------------------#
  def load_everything()
    data = Marshal.load( File.read(@libdata_filename) )
    @calendar.set_date(data[0])
    @members = data[1]
    @collection = data[2]
  end

  private :save_everything, :load_everything
end