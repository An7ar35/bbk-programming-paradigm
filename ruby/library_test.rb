require 'test/unit'
require 'library.rb'

########################
# Calendar Class Tests #
########################
class TestCalendar < Test::Unit::TestCase
  def test_Calendar
    calendar = Calendar.instance
    for i in 0..100
      expected = calendar.get_date()
      assert(expected == i)
      expected = calendar.advance()
      assert(expected == i + 1)
    end
    calendar.set_date( 500 )
    expected = calendar.get_date()
    assert( expected == 500 )
  end
end

####################
# Book Class Tests #
####################
class TestBook < Test::Unit::TestCase
  def test_get_id
    book = Book.new(666, "Necronomicon","Steve")
    assert( book.get_id == 666 )
  end

  def test_get_title
    book = Book.new(666, "Necronomicon","Steve")
    assert( book.get_title == "Necronomicon" )
  end

  def test_get_author
    book = Book.new(666, "Necronomicon","Steve")
    assert( book.get_author == "Steve" )
  end

  def test_get_due_date
    book = Book.new(666, "Necronomicon","Steve")
    book.check_out(123)
    assert( book.get_due_date() == 123 )
  end

  def test_check_out
    book = Book.new(666, "Necronomicon","Steve")
    assert( book.get_due_date() == nil )
    book.check_out(500)
    assert( book.get_due_date()== 500 )
  end

  def test_check_in
    book = Book.new(666, "Necronomicon","Steve")
    book.check_out(10)
    assert( book.get_due_date()== 10 )
    book.check_in()
    assert( book.get_due_date()== nil )
  end

  def test_to_s
    book = Book.new(666, "Necronomicon","Steve")
    assert( book.to_s() == "666: Necronomicon, by Steve")
  end
end

######################
# Member Class Tests #
######################
class TestMember < Test::Unit::TestCase
  def test_get_name
    library = Library.instance
    member = Member.new("John Smith",library)
    assert(member.get_name() == "John Smith")
  end

  def test_check_out
    library = Library.instance
    book = Book.new(666, "Necronomicon","Steve")
    member = Member.new("John Smith",library)
    member.check_out(book)
    assert( member.get_books().include?(book) )
  end

  def test_give_back
    library = Library.instance
    book = Book.new(666, "Necronomicon","Steve")
    member = Member.new("John Smith",library)
    member.check_out(book)
    assert( member.get_books().include?(book) )
    member.give_back(book)
    assert( !( member.get_books().include?(book) ) )
  end

  def test_get_books
    library = Library.instance
    book1 = Book.new(666, "Necronomicon","Steve")
    book2 = Book.new(333,"Bible of the beast","Jeff")
    member = Member.new("John Smith",library)
    member.check_out(book1)
    member.check_out(book2)
    assert( member.get_books().include?(book1) )
    assert( member.get_books().include?(book2) )
  end

  def test_send_overdue_notice
    library = Library.instance
    member = Member.new("John Smith",library)
    member.send_overdue_notice("You're late!")
  end
end

#######################
# Library Class Tests #
#######################
class TestLibrary < Test::Unit::TestCase
  def setup
    @library = Library.instance
  end

  def test01_open
    @library.open()
    assert_raise( RuntimeError ) { @library.open() };
  end

  def test02_close
    assert_nothing_raised() { @library.close() }
    assert_raise( RuntimeError ) { @library.find_all_overdue_books() }
    assert_raise( RuntimeError ) { @library.issue_card( "John Smith" ) }
    assert_raise( RuntimeError ) { @library.serve( "John" ) }
    assert_raise( RuntimeError ) { @library.find_overdue_books() }
    assert_raise( RuntimeError ) { @library.find_all_overdue_books() }
    assert_raise( RuntimeError ) { @library.check_in 1, 2, 3 }
    assert_raise( RuntimeError ) { @library.search( "search term" ) }
    assert_raise( RuntimeError ) { @library.check_out 1, 2, 3 }
    assert_raise( RuntimeError ) { @library.renew  1, 2, 3 }
    assert_raise( RuntimeError ) { @library.close() }
    assert_nothing_raised() { @library.open() }
    assert( @library.close() == "Good night." )
  end

  def test03_issue_card
    @library.open()
    assert_nothing_raised() { @library.issue_card( "John Smith" ) }
    assert( @library.issue_card( "John Smith" ) == "John Smith already has a library card." )
    assert_nothing_raised() { @library.issue_card( "Steve" ) }
  end

  def test04_serve
    assert( @library.serve( "The Dude" ) == "The Dude does not have a library card." )
    assert( @library.serve( "Steve" ) == "Now serving Steve." )
  end

  def test05_search
    expected = "8: All the King's Men, by Robert Penn Warren\n28: Cabbages and Kings, by O. Henry\n113: A Monstrous Regiment of Women, by Laurie R. King\n133: Pale Kings and Princes, by Robert B. Parker\n"
    string = @library.search( "king" )
    assert( string == expected )
    assert( @library.search( "kin" ) == "Search string must contain at least four characters." )
    assert( @library.search( "Something that doesn't exist" ) == "No books found." )
  end

  def test06_check_out
    @library.close()
    @library.open()
    assert_raise( RuntimeError ) { @library.check_out 15, 16, 17 }
    @library.serve( "Steve" )
    assert_raise( RuntimeError ) { @library.check_out 15, 16, 17, 18 }
    assert( ( @library.check_out 15, 16 ) == "2 book(s) have been checked out to Steve." )
    assert_raise( RuntimeError ) { @library.check_out 17, 18 }
    assert( ( @library.check_out 17 ) == "1 book(s) have been checked out to Steve." )
  end

  def test07_find_overdue_books
    for i in  0..3
      @library.close()
      @library.open()
    end
    @library.serve( "John Smith" )
    assert( ( @library.check_out 20, 25 ) == "2 book(s) have been checked out to John Smith." )
    for i in  0..5
      @library.close()
      @library.open()
    end
    assert_raise( RuntimeError ) { @library.find_overdue_books() }
    @library.serve( "Steve" )
    expected = "15: A Time to Kill, by John Grisham\n16: Behold the Man, by Michael Moorcock\n17: Beneath the Bleeding, by Val McDermid\n"
    assert( @library.find_overdue_books() == expected )
  end

  def test08_find_all_overdue_books
    for i in  0..7
      @library.close()
      @library.open()
    end
    expected = "John Smith: 20: Blood's a Rover, by James Ellroy\nJohn Smith: 25: Bury My Heart at Wounded Knee, by Dee Brown\nSteve: 15: A Time to Kill, by John Grisham\nSteve: 16: Behold the Man, by Michael Moorcock\nSteve: 17: Beneath the Bleeding, by Val McDermid\n"
    assert( @library.find_all_overdue_books() == expected )
  end

  def test09_renew
    assert_raise( RuntimeError ) { @library.renew 15 }
    @library.serve( "Steve" )
    assert_raise( RuntimeError ) { @library.renew 100 }
    assert_nothing_raised() { @library.renew 15, 16 }
    expected = "17: Beneath the Bleeding, by Val McDermid\n"
    assert( @library.find_overdue_books() == expected )
  end

  def test10_check_in
    @library.close()
    @library.open()
    assert_raise( RuntimeError ) { @library.check_in 15, 16 }
    @library.serve( "Steve" )
    assert_raise( RuntimeError ) { @library.check_in 100 }
    assert( ( @library.check_in 15, 16 ) == "Steve has returned 2 book(s)." )
  end

  def test11_quit
    assert( @library.quit() == "The library is now closed for renovations." )
  end
end