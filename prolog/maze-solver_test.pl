% Author: Es A. Davison

:-consult('maze-solver.pl').

inBoundary_test :-
	inBoundary([1,1]),
	inBoundary([5,9]),
	inBoundary([4,4]),
	not( inBoundary([0,0]) ),
	not( inBoundary([6,10]) ).

isValidMove_test :-
        isValidMove([1,1]),
	isValidMove([5,9]),
	not( isValidMove([4,4]) ),
	not( isValidMove([0,0]) ),
	not( isValidMove([6,10]) ).

isMatch_test :-
	isMatch([1,1],[1,1]),
	not( isMatch([1,1],[1,2]) ).


aimedMove_test :-
	aimedMove([1,3],[5,9],[X,Y]),
	X =@= 2,
	Y =@= 3.

move_test :-
	move([1,1],[X,Y]),
	X =@= 1,
	Y =@= 2.

solve_test :-
	solve([3,2],[2,6],Path),
	Path ==	[[3,2],[3,3],[2,3],[1,3],[1,4],[1,5],[1,6],[2,6]].


:-	inBoundary_test.
:-	isValidMove_test.
:-	isMatch_test.
:-	aimedMove_test.
:-	move_test.
:-	solve_test.
