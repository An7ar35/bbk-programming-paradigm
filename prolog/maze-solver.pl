% Author: Es A. Davison

:-consult(maze).

% Checks if [X,Y] is within the maze boundaries
inBoundary([X,Y]) :-
	mazeSize(Rows,Columns),
	X =< Rows,
	X > 0,
	Y =< Columns,
	Y > 0.

% Checks if [X,Y] is a valid move
isValidMove([X,Y]) :-
	inBoundary([X,Y]),
	not( barrier(X,Y) ).

% Checks if two coordinates are the same
isMatch([X,Y],[X,Y]).

% Move towards the target coordinate as much as it is possible
aimedMove([X1,Y],[GoalX,_],[X2,Y]) :-
	X1 @< GoalX,
	X2 is X1 + 1,
	isValidMove([X2,Y]).
aimedMove([X1,Y],[GoalX,_],[X2,Y]) :-
	X1 @> GoalX,
	X2 is X1 - 1,
        isValidMove([X2,Y]).
aimedMove([X,Y1],[_,GoalY],[X,Y2]) :-
	Y1 @< GoalY,
	Y2 is Y1 + 1,
	isValidMove([X,Y2]).
aimedMove([X,Y1],[_,GoalY],[X,Y2]) :-
	Y1 @> GoalY,
	Y2 is Y1 - 1,
	isValidMove([X,Y2]).
aimedMove([X1,Y1],[_,_],[X2,Y2]) :-
	% Targeted move not working so go with whatever works...
	move([X1,Y1],[X2,Y2]).

% Move in whatever valid direction works
move([X,Y],[A,Y]) :-
	% Going down
        A is X + 1, isValidMove([A,Y]).
move([X,Y],[A,Y]) :-
	% Going up
	A is X - 1, isValidMove([A,Y]).
move([X,Y],[X,B]) :-
	% Going right
	B is Y + 1, isValidMove([X,B]).
move([X,Y],[X,B]) :-
	% Going left
	B is Y - 1, isValidMove([X,B]).

% Grid eploration
explore(From,To,Visited,Path) :-
	aimedMove(From,To,NextMove),
	(   \+ isMatch(From,To) -> \+ memberchk(NextMove,Visited),
	    append(Visited,[NextMove],NewVisited),
	    explore(NextMove,To,NewVisited,Path)
	;   Path = Visited ).

% Solve function!
solve(From,To,Path) :-
	isValidMove(From),
	isValidMove(To),
	explore(From,To,[From],Path),
	printMaze(Path),
	!.




% Printing the Maze

printMaze(Path) :-
	mazeSize(Height,Width),
	\+ loopColNums(1,Width),
	nl,
	\+ loopFrameEdges(1,Width),
	nl,
	\+ loopVertical(1,Width,Height,Path),
	\+ loopFrameEdges(1,Width),
	nl.

printItem([X,Y],Path) :-
	memberchk([X,Y],Path) -> write('*');
	barrier(X,Y) -> write('X');
	write('.').

loopHorizontal(Start,Width,CurrentRow,Path) :-
	Start < Width + 1,
	succ(Start,Next),
	printItem([CurrentRow,Start],Path),
	write(' '),
	loopHorizontal(Next,Width,CurrentRow,Path).

loopVertical(Start,Width,Height,Path) :-
	Start < Height + 1,
	succ(Start,Next),
	write(Start),write(' | '),
	\+ loopHorizontal(1,Width,Start,Path),
	write('|\n'),
	loopVertical(Next,Width,Height,Path).

loopColNums(Start,Width) :-
	Start < Width + 1,
	succ(Start,Next),
	(   Start =@= 1 -> write('    '), write(Start);
	write(' '),write(Start)),
	loopColNums(Next,Width).

loopFrameEdges(Start,Width) :-
	Start < Width + 1,
	succ(Start,Next),
	(   Start =@= 1 -> write('  +--');
	(   Start =@= Width -> write('---+');
	write('--'))),
	loopFrameEdges(Next,Width).
